# Imports
import sys
import requests
import math
import re
from bs4 import BeautifulSoup

# This is for
def roundup(x):
    return int(math.ceil(x / 10.0)) * 10

results = 0

url_site = "https://tel.local.ch"
for npa in range(1000, 3999):
    try:
        print("#####################NPA IS : " + str(npa))
        # Make a request on 'https://tel.local.ch/fr/q/<NPA>/entreprise'
        html_page = requests.get(url_site + '/fr/q/' + str(npa) + '/entreprise')
        html_txt = html_page.text
        soup = BeautifulSoup(html_txt, 'html.parser')
        # With BeautifulSoup we search the div with 'search-header-results-title' class to get how many results for each NPA
        headerPage = soup.find("div", { "class" : "search-header-results-title" })
        # Roundup the number of results to get the number of pages (10 results by page)
        nbResults = headerPage.text.split()[0]
        nbPages = roundup(float(nbResults)) / 10
        # Loop for all pages in a NPA
        for page in range(1, int(nbPages)):
            # Make a request on 'https://tel.local.ch/fr/q/<NPA>/entreprise?page=<PageNumber>'
            html_page_x = requests.get(url_site + '/fr/q/' + str(npa) + '/entreprise?page=' + str(page))
            html_txt_x = html_page_x.text
            soup_x = BeautifulSoup(html_txt_x, 'html.parser')
            # With BeautifulSoup we sort all div with 'entry-card' and 'entry-card-phonebook'
            for card_x in soup_x.findAll("div", {"class" : "entry-card entry-card-phonebook"}):
                # With BeautifulSoup we sort all span with 'visible-print'
                mail_x = card_x.find("span", {"class" : "visible-print"})
                # Check, if it is an url → pass
                if not re.search("http.*", str(mail_x)) and mail_x != None:
                    print(mail_x.text)
                    results += 1
    except Exception as e:
        print("NEXT !")
    print("#####################FOR " + str(npa) + " WE HAVE " + str(results) + " EMAIL")
print("#####################TOTAL: " + str(results))
